package com.example.android.inventory.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class InvContract {
     /* URI BASE_CONTENT_URI
       *
       * concatenate the CONTENT_AUTHORITY constant with the scheme
       * “content://” we will create the BASE_CONTENT_URI
       * which will be shared by every URI associated with InvContract.
       * */

    public static  final String CONTENT_AUTHORITY = "com.example.android.inventory";
    /*use the parse method which takes in a URI string and returns Uri*/
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    /* PATH_TableName
    *
    * This constants stores the path for each of the tables
    * which will be appended to the base content URI.
    * */
    public static final String PATH_INVENTORY = "inventory";


    /* Inner class that defines the table contents */
    public static abstract class InvEntry implements BaseColumns {

        /* Complete CONTENT_URI
         *
         * The content URI to access the inventory data in the provider
         *
         * The Uri.withAppendedPath() method appends the BASE_CONTENT_URI,
         * (which contains the scheme and the content authority) to the path segment.
         **/
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_INVENTORY);

        /** Name of database table for inventory */
        public static final String TABLE_NAME = "inventory";
        /**
         * Unique ID number for the pet (only for use in the database table).
         *
         * Type: INTEGER
         */
        public static final String _ID = BaseColumns._ID;

        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_QUANTITY = "quantity";
        public static final String COLUMN_SUPPLIER = "supplier";
        public static final String COLUMN_PRICE = "price";
        public static final String COLUMN_PICTURE = "image";

        /**
         * Possible values for the supplier of the inventory items.
         *INTEGER (one of 3 gender types 0 = banggood, 1 = abc-rc, 2 = botland
         * */
        public static final int BANGGOOD = 0;
        public static final int ABC_RC = 1;
        public static final int BOTLAND = 2;

        public static boolean isSupplierValid(int supplier) {
            if (
                    supplier == BANGGOOD ||
                            supplier == ABC_RC ||
                            supplier == BOTLAND) {
                return true;
            }
            return false;
        }
        /**
         * The MIME type of the CONTENT_URI for a inventory list.
         */
        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_INVENTORY;

        /**
         * The MIME type of the CONTENT_URI for a single inventory item.
         */
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_INVENTORY;

    }
}
