package com.example.android.inventory;

import android.content.ContentResolver;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.android.inventory.data.InvContract;

import java.util.Set;

import static android.R.attr.id;
import static com.example.android.inventory.data.InvContract.InvEntry;
import static java.lang.Integer.parseInt;
import static java.security.AccessController.getContext;

/**
 * Created by cini on 02.11.2017.
 */

public class InvCursorAdapter extends CursorAdapter {
    /**
     * Constructs a new {@link InvCursorAdapter}.
     *
     * @param context The context
     * @param c       The cursor from which to get the data.
     */
    public InvCursorAdapter(Context context, Cursor c) {
        super(context, c, 0 /* flags */);
    }

    /**
     * Makes a new blank list item view. No data is set (or bound) to the views yet.
     *
     * @param context app context
     * @param cursor  The cursor from which to get the data. The cursor is already
     *                moved to the correct position.
     * @param parent  The parent to which the new view is attached to
     * @return the newly created list item view.
     */
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // Inflate a list item view using the layout specified in list_item.xml


        return LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);

    }

    /**
     * This method binds the item data (in the current row pointed to by cursor) to the given
     * list item layout. For example, the name for the current product can be set on the name TextView
     * in the list item layout.
     *
     * @param view    Existing view, returned earlier by newView() method
     * @param context app context
     * @param cursor  The cursor from which to get the data. The cursor is already moved to the
     * correct row.
     */
    @Override

    public void bindView(final View view, final Context context, final Cursor cursor) {

        // Find individual views that we want to modify in the list item layout
        TextView tvProductName = (TextView) view.findViewById(R.id.product_name);
        TextView tvProductQuantity = (TextView) view.findViewById(R.id.product_quantity);
        TextView tvProductPrice = (TextView) view.findViewById(R.id.product_price);
        Log.v("takmyslalem", String.valueOf(cursor.getLong(cursor.getColumnIndex("_id"))));

        // Find the columns of inventory item attributes that we're interested in
        int nameColumnIndex = cursor.getColumnIndex(InvEntry.COLUMN_NAME);
        final int quantityColumnIndex = cursor.getColumnIndex(InvEntry.COLUMN_QUANTITY);
        int priceColumnIndex = cursor.getColumnIndex(InvEntry.COLUMN_PRICE);
        int itemId = cursor.getColumnIndex("_id");


        // Read the inventory item attributes from the Cursor for the current item
        String itemName = cursor.getString(nameColumnIndex);
        String itemQuantityString = cursor.getString(quantityColumnIndex);
        final int itemQuantity = parseInt(itemQuantityString);
        String itemPrice = cursor.getString(priceColumnIndex);
        long id = cursor.getLong(itemId);


        final int rowId = cursor.getInt(cursor.getColumnIndex(InvEntry._ID));
        Log.v("rowID", "current row id= " + rowId);
        // Set on click listener to decrease stock status
        Button bSaleButton = (Button) view.findViewById(R.id.btn_sale);

        bSaleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty = itemQuantity;
                if (qty > 0) {
                    qty = qty - 1;
                }
                ContentValues values = new ContentValues();
                values.put(InvEntry.COLUMN_QUANTITY, qty);
                Uri currentUri = ContentUris.withAppendedId(InvEntry.CONTENT_URI, rowId);
                Log.v("newUri", "currentUri= " + currentUri);
                context.getContentResolver().update(currentUri, values, null, null);
                Log.v("test", "quantity= " + qty);
            }

        });




        // Update the TextViews with the attributes for the current inventory item
        tvProductName.setText(itemName);
        tvProductQuantity.setText(String.valueOf(itemQuantity));
        tvProductPrice.setText(itemPrice);


    }


}
