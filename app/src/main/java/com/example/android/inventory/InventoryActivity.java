package com.example.android.inventory;

import android.app.LoaderManager;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.UriMatcher;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.inventory.data.InvContract.InvEntry;

public class InventoryActivity extends AppCompatActivity implements  LoaderManager.LoaderCallbacks<Cursor>{
    private  static final int INV_LOADER = 0;
    InvCursorAdapter mCursorAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(InventoryActivity.this, DetailActivity.class);
                startActivity(intent);
            }
        });
        // Find the ListView which will be populated with the inventory item data
        ListView invListView = (ListView) findViewById(R.id.list);
        // Find and set empty view on the ListView, so that it only shows when the list has 0 items.
        View emptyView = findViewById(R.id.empty_view);
        invListView.setEmptyView(emptyView);
        // Setup an Adapter to create a list item for each row of item data in the Cursor.
        mCursorAdapter = new InvCursorAdapter(this, null);
        // Attach the adapter to the ListView.
        invListView.setAdapter(mCursorAdapter);

        // Setup the list item click
        invListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /*
            * @param adapterView is the list view
            * @param view is the view for the item
            * @param position is the position of the item in the listview
            * @param id is the ID of the item
            * */
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                // Find the currentitem that was clicked on
                // Get the clicked row with id
                Uri currentItemUri = ContentUris.withAppendedId(InvEntry.CONTENT_URI, id);

                Intent intent = new Intent(InventoryActivity.this, DetailActivity.class);
                intent.setData(currentItemUri);
                Log.v("CURRENT_ITEM_URI", currentItemUri.toString());
                // Send the intent to launch a new activity
                startActivity(intent);
            }


        });


        // Start the loader
        getLoaderManager().initLoader(INV_LOADER, null, this);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {

        String[] projection = {
                InvEntry._ID,
                InvEntry.COLUMN_NAME,
                InvEntry.COLUMN_QUANTITY,
                InvEntry.COLUMN_PRICE
        };
        return new CursorLoader(this, InvEntry.CONTENT_URI, projection, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mCursorAdapter.swapCursor(null);
    }

}

