package com.example.android.inventory.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.example.android.inventory.data.InvContract.InvEntry;

public class InvDbHelper extends SQLiteOpenHelper{


    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "inventory.db";

    public InvDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        /* Create TABLE inventory (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL,
        *  quantity INTEGER NOT NULL DEFAULT 0, supplier INTEGER NOT NULL, price PICTURE TEXT);
        *  Create a String that contains the SQL statement to create inventory table */
        String SQL_CREATE_INVENTORY_TABLE = "CREATE TABLE " + InvEntry.TABLE_NAME + " (" +
                InvEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                InvEntry.COLUMN_NAME + " TEXT NOT NULL," +
                InvEntry.COLUMN_QUANTITY + " INTEGER NOT NULL DEFAULT 0," +
                InvEntry.COLUMN_SUPPLIER + " INTEGER NOT NULL," +
                InvEntry.COLUMN_PRICE + " INTEGER NOT NULL," +
                InvEntry.COLUMN_PICTURE + " TEXT);";
        Log.v("TABELA", SQL_CREATE_INVENTORY_TABLE);

        db.execSQL(SQL_CREATE_INVENTORY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // Gets the data repository in write mode

    }
}

