package com.example.android.inventory;


import android.app.LoaderManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import com.example.android.inventory.data.InvContract;
import com.example.android.inventory.data.InvContract.InvEntry;

import static android.R.attr.name;
import static com.example.android.inventory.R.id.fab;
import static com.example.android.inventory.R.id.send_order_btn;
import static com.example.android.inventory.R.id.view_offset_helper;
import static com.example.android.inventory.R.string.quantity;

public class DetailActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    /**
     * Identifier for the pet data loader
     */
    private static final int EXISTING_INV_LOADER = 0;

    /**
     * Content URI for the existing item (null if it's a new item)
     */
    private Uri mCurrentItemUri;

    /**
     * EditText field to enter the item's name
     */
    private EditText mItemNameEditText;

    /**
     * EditText field to enter the item's quantity
     */
    private EditText mQuantityEditText;

    /**
     * EditText field to enter the item's supplier
     */
    private Spinner mSupplierSpinner;

    /**
     * EditText field to enter the item's price
     */
    private EditText mPriceEditText;

    /**
     * EditText field to enter the item's image URI
     */
    private String mImageUri;
    /**
     * Item supplier's name
     */
    private String mSupplierName;

    private boolean mItemHasChanged = false;
    /**
     * Item supplier. The possible values are:
     * 0 for Bangood, 1 for ABC_RC, 2 for BOTLAND.
     */
    private int mSupplier = 0;


    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            mItemHasChanged = true;
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        // Examin the intent that was used to lunch this activity,
        // in rder to figure out if we're creating new iten or editing th existing one.
        Intent intent = getIntent();
        mCurrentItemUri = intent.getData();


        // If the intent DOES NOT contain a pet content URI, then we know that we are
        // creating a new pet.
        if (mCurrentItemUri == null) {
            //This is a new item, so change app bar to say "Add new Item"
            setTitle(getString(R.string.editor_activity_title_new_item));
            // Invalidate the options menu, so the "Delete" menu option can be hidden.
            // (It doesn't make sense to delete a item that hasn't been created yet.)
            invalidateOptionsMenu();
        } else {
            //This is an existing item, so change app bar to say "Edit Item"
            setTitle(getString(R.string.editor_activity_title_edit_item));
            // Start the loader
            getLoaderManager().initLoader(EXISTING_INV_LOADER, null, this);
        }
        // Find all relevant views that we will need to read user input from
        mItemNameEditText = (EditText) findViewById(R.id.edit_item_name);
        mQuantityEditText = (EditText) findViewById(R.id.edit_item_quantity);
        mPriceEditText = (EditText) findViewById(R.id.edit_item_price);
        mSupplierSpinner = (Spinner) findViewById(R.id.spinner_supplier);

        mItemNameEditText.setOnTouchListener(mTouchListener);
        mQuantityEditText.setOnTouchListener(mTouchListener);
        mPriceEditText.setOnTouchListener(mTouchListener);
        mSupplierSpinner.setOnTouchListener(mTouchListener);


        setupSpinner();

        Button order = (Button) findViewById(R.id.send_order_btn);
        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name) + "'s order.");
                intent.putExtra(Intent.EXTRA_TEXT, createOrderSummary(R.string.app_name));
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }

            }
        });

        Button shipment = (Button) findViewById(R.id.quantity_plus_btn);
        shipment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                increment(Integer.parseInt(mQuantityEditText.getText().toString().trim()));
                saveItem();

            }

        });

        Button sale = (Button) findViewById(R.id.quantity_minus_btn);
        sale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decrement(Integer.parseInt(mQuantityEditText.getText().toString().trim()));
                saveItem();

            }
        });
    }

    /**
     * This method is called when the plus button is clicked.
     */
    public void increment(int quantityInt) {
        if (quantityInt >= 0) {
            quantityInt += 1;
            mQuantityEditText.setText(String.valueOf(quantityInt));

        }
    }


    /**
     * This method is called when the minus button is clicked.
     */
    public void decrement(int quantityInt) {
        if (quantityInt > 0) {
            quantityInt -= 1;
            Log.v("quantityINT", "quantity INT po odjeciu: " + String.valueOf(quantityInt));
            mQuantityEditText.setText(String.valueOf(quantityInt));
        }
    }

    private String createOrderSummary(int appName) {
        String orderMessage = getString(R.string.orderFrom) + "  " + getString(R.string.app_name) + "  " + getString(R.string.orderTo) + "  " + mSupplierName;
        orderMessage += "\n" + getString(R.string.product) + " " + mItemNameEditText.getText().toString().trim();
        orderMessage += "\n" + getString(R.string.quantity) + " " + mQuantityEditText.getText().toString().trim();
        orderMessage += "\n" + getString(R.string.price) + " " + mPriceEditText.getText().toString().trim();
        orderMessage += "\n" + getString(R.string.sum_price) + " " + Integer.parseInt(mPriceEditText.getText().toString().trim()) *
                Integer.parseInt(mQuantityEditText.getText().toString().trim());
        orderMessage += " " + getString(R.string.moneyUnits);

        return orderMessage;
    }

    /**
     * Setup the dropdown spinner that allows the user to select the supplier of the inventory item.
     */
    private void setupSpinner() {
        // Create adapter for spinner. The list options are from the String array it will use
        // the spinner will use the default layout
        ArrayAdapter supplierSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_supplier_options, android.R.layout.simple_spinner_item);

        // Specify dropdown layout style - simple list view with 1 item per line
        supplierSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        // Apply the adapter to the spinner
        mSupplierSpinner.setAdapter(supplierSpinnerAdapter);

        // Set the integer mSelected to the constant values
        mSupplierSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selection)) {
                    if (selection.equals(getString(R.string.supplier_abc_rc))) {
                        mSupplier = InvEntry.ABC_RC; // ABC-RC
                        mSupplierName = getString(R.string.supplier_abc_rc);
                    } else if (selection.equals(getString(R.string.supplier_botland))) {
                        mSupplier = InvEntry.BOTLAND; // BotLand
                        mSupplierName = getString(R.string.supplier_botland);
                    } else {
                        mSupplier = InvEntry.BANGGOOD; // Banggood
                        mSupplierName = getString(R.string.supplier_banggood);
                    }
                }
            }

            // Because AdapterView is an abstract class, onNothingSelected must be defined
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mSupplier = 0; // Banggood
            }
        });
    }

    // Get user input from editor and save item into inventory database.
    public void saveItem() {

        String inv_item_name = mItemNameEditText.getText().toString().trim();
        String inv_item_quantity = mQuantityEditText.getText().toString().trim();
        String inv_item_price = mPriceEditText.getText().toString().trim();
        String inv_item_supplier = String.valueOf(mSupplier);
        String inv_item_picture;

        // Create a ContentValues object where column names are the keys,
        // and item attributes from the editor are the values.
        ContentValues values = new ContentValues();
        values.put(InvEntry.COLUMN_NAME, inv_item_name);
        values.put(InvEntry.COLUMN_SUPPLIER, mSupplier);


        // If the price and/or is not provided by the user, don't try to parse the string into an
        // integer value. Use 0 by default.
        int price = 0;
        int quantity = 0;

        if (!TextUtils.isEmpty(inv_item_price)) {
            price = Integer.parseInt(inv_item_price);

        }
        if (!TextUtils.isEmpty(inv_item_quantity)) {
            quantity = Integer.parseInt(inv_item_quantity);


        }
        values.put(InvEntry.COLUMN_PRICE, price);
        values.put(InvEntry.COLUMN_QUANTITY, quantity);

        // Determine if this is a new or existing item by checking if mCurrentItemUri is null or not
        if (mCurrentItemUri == null) {
            if (
                // Check for all empty ot null values
                    TextUtils.isEmpty(inv_item_name) ||
                            TextUtils.isEmpty(inv_item_quantity) ||
                            TextUtils.isEmpty(inv_item_price) ||
                            !InvEntry.isSupplierValid(mSupplier)
                    ) {

                //Exit activity in that case
                return;
            }
            // This is a NEW item, so insert a new item into the provider,
            // returning the content URI for the new item.
            Uri newUri = getContentResolver().insert(InvEntry.CONTENT_URI, values);

            // Show a toast message depending on whether or not the insertion was successful.
            if (newUri == null) {
                // If the new content URI is null, then there was an error with insertion.
                Toast.makeText(this, getString(R.string.editor_insert_item_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                // Otherwise, the insertion was successful and we can display a toast.
                Toast.makeText(this, getString(R.string.editor_insert_item_successful),
                        Toast.LENGTH_SHORT).show();
            }

        } else {
            // Otherwise this is an EXISTING item, so update the item with content URI: mCurrentItemUri
            // and pass in the new ContentValues. Pass in null for the selection and selection args
            // because mCurrentItemUri will already identify the correct row in the database that
            // we want to modify.
            int rowsAffected = getContentResolver().update(mCurrentItemUri, values, null, null);
            // Show a toast message depending on whether or not the update was successful.
            if (rowsAffected == 0) {
                // If no rows were affected, then there was an error with the update.
                Toast.makeText(this, getString(R.string.editor_update_item_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                // Otherwise, the update was successful and we can display a toast.
                Toast.makeText(this, getString(R.string.editor_update_item_successful),
                        Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        // If this is a new item, hide the "Delete" menu item.
        if (mCurrentItemUri == null) {
            MenuItem menuItem = menu.findItem(R.id.action_delete);
            menuItem.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option
            case R.id.action_save:
                // Save item data into database
                saveItem();
                // Exit activity
                finish();
                return true;
            // Respond to a click on the "Delete" menu option
            case R.id.action_delete:
                // Delete item
                deleteItem();
                // Exit activity
                finish();
                return true;
            // Respond to a click on the "Up" arrow button in the app bar
            case android.R.id.home:
                // If the pet hasn't changed, continue with navigating up to parent activity
                // which is the {@link CatalogActivity}.
                if (!mItemHasChanged) {
                    NavUtils.navigateUpFromSameTask(DetailActivity.this);
                    return true;
                }

                // Otherwise if there are unsaved changes, setup a dialog to warn the user.
                // Create a click listener to handle the user confirming that
                // changes should be discarded.
                DialogInterface.OnClickListener discardButtonClickListener =
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // User clicked "Discard" button, navigate to parent activity.
                                NavUtils.navigateUpFromSameTask(DetailActivity.this);
                            }
                        };

                // Show a dialog that notifies the user they have unsaved changes
                showUnsavedChangesDialog(discardButtonClickListener);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        // Since the editor shows all item attributes, define a projection that contains
        // all columns from the inventory table
        String[] projection = {
                InvEntry._ID,
                InvEntry.COLUMN_NAME,
                InvEntry.COLUMN_QUANTITY,
                InvEntry.COLUMN_SUPPLIER,
                InvEntry.COLUMN_PRICE,
                InvEntry.COLUMN_PICTURE
        };

        // This loader will execute the ContentProvider's query method on a background thread
        return new CursorLoader(this,   // Parent activity context
                mCurrentItemUri,         // Query the content URI for the current item
                projection,             // Columns to include in the resulting Cursor
                null,                   // No selection clause
                null,                   // No selection arguments
                null);                  // Default sort order
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        // Bail early if the cursor is null or there is less than 1 row in the cursor
        if (cursor == null || cursor.getCount() < 1) {
            return;
        }

        // Proceed with moving to the first row of the cursor and reading data from it
        // (This should be the only row in the cursor)
        if (cursor.moveToFirst()) {
            // Find the columns of pet attributes that we're interested in
            int nameColumnIndex = cursor.getColumnIndex(InvEntry.COLUMN_NAME);
            int quantityColumnIndex = cursor.getColumnIndex(InvEntry.COLUMN_QUANTITY);
            int supplierColumnIndex = cursor.getColumnIndex(InvEntry.COLUMN_SUPPLIER);
            int priceColumnIndex = cursor.getColumnIndex(InvEntry.COLUMN_PRICE);

            // Extract out the value from the Cursor for the given column index
            String name = cursor.getString(nameColumnIndex);
            int quantity = cursor.getInt(quantityColumnIndex);
            int supplier = cursor.getInt(supplierColumnIndex);
            int price = cursor.getInt(priceColumnIndex);
            String quanityString = String.valueOf(quantity);
            String priceString = String.valueOf(price);
            // Update the views on the screen with the values from the database
            mItemNameEditText.setText(name);
            mQuantityEditText.setText(quanityString);
            mSupplierSpinner.setSelection(supplier);
            mPriceEditText.setText(priceString);

            Log.v("aktualizuje01", String.valueOf(cursor.getLong(cursor.getColumnIndex(InvEntry._ID))));
            // Supplier is a dropdown spinner, so map the constant value from the database
            // into one of the dropdown options (0 is Banggood, 1 is ABC-RC, 2 is Botland).
            // Then call setSelection() so that option is displayed on screen as the current selection.
            switch (supplier) {
                case InvEntry.ABC_RC:
                    mSupplierSpinner.setSelection(1);
                    break;
                case InvEntry.BOTLAND:
                    mSupplierSpinner.setSelection(2);
                    break;
                default:
                    mSupplierSpinner.setSelection(0);
                    break;
            }


        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // If the loader is invalidated, clear out all the data from the input fields.
        mItemNameEditText.setText("");
        mQuantityEditText.setText("");
        mSupplierSpinner.setSelection(0);// Select "Banggod" gender
        mPriceEditText.setText("");
    }

    private void showUnsavedChangesDialog(
            DialogInterface.OnClickListener discardButtonClickListener) {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the positive and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.unsaved_changes_dialog_msg);
        builder.setPositiveButton(R.string.discard, discardButtonClickListener);
        builder.setNegativeButton(R.string.keep_editing, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Keep editing" button, so dismiss the dialog
                // and continue editing the item.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    private void showDeleteConfirmationDialog() {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the postivie and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_dialog_msg);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Delete" button, so delete the item.
                deleteItem();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Cancel" button, so dismiss the dialog
                // and continue editing the item.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /**
     * Perform the deletion of the item in the database.
     */
    private void deleteItem() {
        int rowsdeleted = getContentResolver().delete(mCurrentItemUri, null, null);// Show a toast message depending on whether or not the update was successful.
        if (rowsdeleted == 0) {
            // If no rows were deleted, then there was an error with the update.
            Toast.makeText(this, getString(R.string.editor_delete_item_failed),
                    Toast.LENGTH_SHORT).show();
        } else {
            // Otherwise, the update was successful and we can display a toast.
            Toast.makeText(this, getString(R.string.editor_delete_item_successful),
                    Toast.LENGTH_SHORT).show();
        }
    }
}
